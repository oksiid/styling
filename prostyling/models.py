from django.db import models
from django.dispatch import receiver
# Create your models here.
from django.db.models.signals import post_delete


class carusel(models.Model):
    number = models.IntegerField()
    photo = models.ImageField(upload_to ='carusel/')

@receiver(post_delete, sender=carusel)
def submission_delete(sender, instance, **kwargs):
    instance.photo.delete(False)

class otziv(models.Model):
    author_name = models.CharField(max_length=50)
    text = models.CharField(max_length=500)
    date = models.CharField(max_length=20, default='')
    rating = models.IntegerField(default=0)
    moderate = models.BooleanField(default=False)
    comment = models.CharField(max_length=500 ,default='')


class raboti(models.Model):
    opisanie = models.CharField(max_length=100)
    text = models.TextField()
    date = models.CharField(max_length=20)

class raboti_photo(models.Model):
    photo_raboti = models.ForeignKey(raboti, on_delete=models.CASCADE, blank=True)
    photo = models.ImageField(upload_to='photos/', blank=True)

@receiver(post_delete, sender=raboti_photo)
def submission_delete(sender, instance, **kwargs):
    instance.photo.delete(False)

class test_model(models.Model):
    name = models.CharField(max_length=100)
    balance = models.IntegerField(default=0)