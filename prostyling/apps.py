from django.apps import AppConfig


class ProstylingConfig(AppConfig):
    name = 'prostyling'
