from django.core.mail import send_mail
from django.http import HttpResponseRedirect
from django.shortcuts import render
from prostyling.models import *
from django.urls import reverse
from django.conf import settings
from django.contrib import messages
from django.contrib.auth import authenticate, login
from django.core.files.base import ContentFile
from .forms import *
from django.core.cache import cache
import pickle

# Create your views here.

def index(request):

    if not cache.get(request.COOKIES['sessionid']):
        pokaz = True
        cache.set(request.COOKIES['sessionid'], 'hello_world', 600)
    else:
        pokaz = False

    context = {
        'otziv':otziv.objects.filter(moderate=True).order_by('-id',)[:4],
        'raboti':raboti.objects.order_by('-id')[:8],
        'pokaz':pokaz
    }

    return render(request, 'index.html', context)

def recall(request):
    if request.method == 'POST':
        messages.add_message(request, messages.INFO, 'Hello world.')
        text = str(request.POST)
        send_mail('Новая запись', text, settings.EMAIL_HOST_USER, ['p.styling@yandex.ru'])
    return HttpResponseRedirect(reverse('index'))



def works(request):
    context = {
        'raboti': raboti.objects.all()
    }
    return render(request, 'works.html', context)

def works_detail(request, raboti_id):
    context={
        'r':raboti.objects.get(id=raboti_id)
    }
    return render(request ,'raboti.html', context)

def reviews(request):
    context = {
        'otziv': otziv.objects.filter(moderate=True).order_by('-id')

    }
    return render(request ,'reviews.html', context)

def rev_add(request):
    if request.method == 'POST':

        new_otz = otziv(author_name=request.POST['name_author'], text=request.POST['text'], rating=int(request.POST['r_1']), date=request.POST['date_rev'])
        new_otz.save()
    return HttpResponseRedirect(reverse('reviews'))

def test(request):
    queryset = test_model.objects.filter(id=1)
    print(queryset.query)

    if request.method == 'POST':
        q = request.POST['query']

        q_ready = test_model.objects.raw(q)

        context = {
            'q':q_ready
        }
        print(q_ready)
        return render(request,'test.html', context)
    else:
        return render(request, 'test.html')


def detailing_car_wash(request):
    if request.method == 'POST':
        messages.add_message(request, messages.INFO, 'Hello world.')
        text = str(request.POST)
        send_mail('Новая запись', text, settings.EMAIL_HOST_USER, ['p.styling@yandex.ru'])
        return HttpResponseRedirect(reverse('detailing_car_wash'))
    return render(request, 'detailing_car_wash.html')

def detailing_motor_wash(request):
    if request.method == 'POST':
        messages.add_message(request, messages.INFO, 'Hello world.')
        text = str(request.POST)
        send_mail('Новая запись', text, settings.EMAIL_HOST_USER, ['p.styling@yandex.ru'])
        return HttpResponseRedirect(reverse('detailing_motor_wash'))
    return render(request, 'detailing_motor_wash.html')

def polirovka_kuzova(request):
    if request.method == 'POST':
        messages.add_message(request, messages.INFO, 'Hello world.')
        text = str(request.POST)
        send_mail('Новая запись', text, settings.EMAIL_HOST_USER, ['p.styling@yandex.ru'])
        return HttpResponseRedirect(reverse('polirovka_kuzova'))
    return render(request, 'polirovka_kuzova.html')


def pokritie_keramica(request):
    if request.method == 'POST':
        messages.add_message(request, messages.INFO, 'Hello world.')
        text = str(request.POST)
        send_mail('Новая запись', text, settings.EMAIL_HOST_USER, ['p.styling@yandex.ru'])
        return HttpResponseRedirect(reverse('pokritie_keramica'))
    return render(request, 'pokritie_keramica.html')

def pokritie_steklo(request):
    if request.method == 'POST':
        messages.add_message(request, messages.INFO, 'Hello world.')
        text = str(request.POST)
        send_mail('Новая запись', text, settings.EMAIL_HOST_USER, ['p.styling@yandex.ru'])
        return HttpResponseRedirect(reverse('pokritie_steklo'))
    return render(request, 'pokritie_steklo.html')

def pokritie_antidojd(request):
    if request.method == 'POST':
        messages.add_message(request, messages.INFO, 'Hello world.')
        text = str(request.POST)
        send_mail('Новая запись', text, settings.EMAIL_HOST_USER, ['p.styling@yandex.ru'])
        return HttpResponseRedirect(reverse('pokritie_antidojd'))
    return render(request, 'pokritie_antidojd.html')

def polirovka_optiki(request):
    if request.method == 'POST':
        messages.add_message(request, messages.INFO, 'Hello world.')
        text = str(request.POST)
        send_mail('Новая запись', text, settings.EMAIL_HOST_USER, ['p.styling@yandex.ru'])
        return HttpResponseRedirect(reverse('polirovka_optiki'))
    return render(request, 'polirovka_optiki.html')

def himchistka(request):
    if request.method == 'POST':
        messages.add_message(request, messages.INFO, 'Hello world.')
        text = str(request.POST)
        send_mail('Новая запись', text, settings.EMAIL_HOST_USER, ['p.styling@yandex.ru'])
        return HttpResponseRedirect(reverse('himchistka'))
    return render(request, 'himchistka.html')

def zashchitnye_pokrytiya_leather(request):
    if request.method == 'POST':
        messages.add_message(request, messages.INFO, 'Hello world.')
        text = str(request.POST)
        send_mail('Новая запись', text, settings.EMAIL_HOST_USER, ['p.styling@yandex.ru'])
        return HttpResponseRedirect(reverse('zashchitnye_pokrytiya_leather'))
    return render(request, 'zashchitnye_pokrytiya_leather.html')

def zashchitnye_pokrytiya_tex(request):
    if request.method == 'POST':
        messages.add_message(request, messages.INFO, 'Hello world.')
        text = str(request.POST)
        send_mail('Новая запись', text, settings.EMAIL_HOST_USER, ['p.styling@yandex.ru'])
        return HttpResponseRedirect(reverse('zashchitnye_pokrytiya_tex'))
    return render(request, 'zashchitnye_pokrytiya_tex.html')

def polirovka_interier(request):
    if request.method == 'POST':
        messages.add_message(request, messages.INFO, 'Hello world.')
        text = str(request.POST)
        send_mail('Новая запись', text, settings.EMAIL_HOST_USER, ['p.styling@yandex.ru'])
        return HttpResponseRedirect(reverse('polirovka_interier'))
    return render(request, 'polirovka_interier.html')

def ozonirovanie(request):
    if request.method == 'POST':
        messages.add_message(request, messages.INFO, 'Hello world.')
        text = str(request.POST)
        send_mail('Новая запись', text, settings.EMAIL_HOST_USER, ['p.styling@yandex.ru'])
        return HttpResponseRedirect(reverse('ozonirovanie'))
    return render(request, 'ozonirovanie.html')

def add_raboty(request):
    context={
        'raboti_form': raboti_form,
        'raboti_photo_form': raboti_photo_form,
    }
    if request.method == 'POST':
        form = raboti_form(request.POST, request.FILES)
        if form.is_valid():
            new_form = form.save(commit=False)
            new_form.save()

            for f in request.FILES.getlist('photos'):
                data = f.read()
                photo = raboti_photo()
                photo.photo_raboti = new_form
                photo.photo.save(f.name, ContentFile(data))
                photo.save()

            return HttpResponseRedirect(reverse('add_raboty'))
        else:
            print(form.errors)
    return render(request, 'add_raboty.html', context)

def login_to(request):

    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(request, username=username, password=password)
    if user is not None:
        login(request, user)
        return HttpResponseRedirect(reverse('add_raboty'))

    else:
        return HttpResponseRedirect(reverse('add_raboty'))

    return HttpResponseRedirect(reverse('add_raboty'))
