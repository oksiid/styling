from django.contrib import admin
from prostyling.models import *



class raboti_photoInline(admin.TabularInline):
    fk_name = 'photo_raboti'
    model = raboti_photo

class raboti_admin(admin.ModelAdmin):
    inlines = [raboti_photoInline]

admin.site.register(raboti, raboti_admin)
admin.site.register(raboti_photo)
admin.site.register(carusel)
admin.site.register(otziv)
admin.site.register(test_model)