from django import forms
from django.forms import Textarea ,FileInput, CharField
from .models import *

class raboti_form(forms.ModelForm):
    class Meta:
        model = raboti
        fields = '__all__'

class raboti_photo_form(forms.Form):
    photos = forms.ImageField(required=False, widget=forms.FileInput(attrs={'multiple': 'multiple', 'class':' custom-file-input', 'id': 'customFile',}))
