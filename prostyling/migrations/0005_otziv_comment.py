# Generated by Django 2.2.4 on 2020-07-26 11:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('prostyling', '0004_otziv_moderate'),
    ]

    operations = [
        migrations.AddField(
            model_name='otziv',
            name='comment',
            field=models.CharField(default='', max_length=500),
        ),
    ]
