$('#dabtn1').click(function () {
        $('#zapis-title').html('Выберите услугу');
        $('#zapis-body').html('<div class="list-group">\n' +
            '  <button id="himkabtn1" type="button" class="list-group-item list-group-item-action">\n' +
            '    Химчистка авто\n' +
            '  </button>\n' +
            '  <button id="polirovkabtn1" type="button" class="list-group-item list-group-item-action">Полировка авто</button>\n' +
            '  <button id="krytexbtn1" type="button" class="list-group-item list-group-item-action">Нанесение защитного покрытия</button>\n' +
            '  <button id="anotherbtn" type="button" class="list-group-item list-group-item-action">Другое</button>\n' +
            '</div>');
    });


    $('#exampleModalCenter1').on('click','#himkabtn1', function () {
        $('#zapis-title').html('Какая химчистка вас интересует');
        $('#zapis-body').html('<div class="row">\n' +
            '                    <button id="himkabtn2" type="button" class="col-6 btn btn-primary">Полная</button>\n' +
            '                    <button id="himkabtn2NO" type="button" class="col-6 btn btn-secondary">Частичная</button>\n' +
            '                </div>');
    });


    $('#exampleModalCenter1').on('click', '#himkabtn2NO',function () {
        $('#zapis-title').html('Заказать обратный звонок');
        $('#zapis-body').html(' <form action="/recall" method="post">\n' +
            '<input name="csrfmiddlewaretoken" hidden value="'+csrftoken+'" >'+
            '<input name="uslyga" hidden value="Химчистка конкретной детали" >'+
            '                      <div class="form-group">\n' +
            '                        <label for="namehimka">Имя</label>\n' +
            '                        <input name="namehimka" type="text" class="form-control" id="namehimka" aria-describedby="emailHelp">\n' +
            '                      </div>\n' +
            '                      <div class="form-group">\n' +
            '                        <label for="phonehimka">Номер телефона</label>\n' +
            '                        <input name="phonehimka" type="text" class="form-control" id="phonehimka">\n' +
            '                      </div>\n' +
            '                      <div class="form-group">\n' +
            '                        <label for="autohimka">Марка и модель авто</label>\n' +
            '                        <input name="autohimka" type="text" class="form-control" id="autohimka">\n' +
            '                      </div>\n' +
            '\n' +
            '                      <button type="submit" class="btn btn-primary">Отправить</button>\n' +
            '                      <div id="rec_google">\n' +
            '                      <div class="g-recaptcha" data-sitekey="6Ld438cZAAAAABfP5Q-xhv95DKPeQVYCGSP9189-"></div>\n' +
            '                      </div>\n' +
            '                </form>');


    });

    var himkaprice;

    $('#exampleModalCenter1').on('click','#himkabtn2', function () {
        $('#zapis-title').html('Выберите класс авто');
        $('#zapis-body').html('<div class="container">\n' +
            '    <div class="input-group">\n' +
            '      <select class="custom-select" id="selecthimka" aria-label="Example select with button addon">\n' +
            '        <option selected>Выбор...</option>\n' +
            '        <option value="1">1 класс</option>\n' +
            '        <option value="2">2 класс</option>\n' +
            '        <option value="3">3 класс</option>\n' +
            '        <option value="4">4 класс</option>\n' +
            '        <option value="5">5 класс</option>\n' +
            '      </select>\n' +
            '      <div class="input-group-append">\n' +
            '        <button id="himkabtn3" class="btn btn-outline-secondary" type="button">Далее</button>\n' +
            '      </div>\n' +
            '    </div>\n' +
            '        <ul class="list-group">\n' +
            '          <li class="list-group-item">1 класс (Kia Picanto, Hyundai Getz, Ford Fiesta, Chevrolet Spark, Citroen C1, C2c, Daewoo Matiz, Peugeot 107, 108)</li>\n' +
            '          <li class="list-group-item">2 класс (Kia Rio Ceed, Hyundai Solaris, Skoda Fabia, BMW 1-3)</li>\n' +
            '          <li class="list-group-item">3 класс (Skoda Octavia, Superb, BMW 4 5, X1-X4, Toyota Rav-4, Kia Sportage)</li>\n' +
            '          <li class="list-group-item">4 класс (BMW 7 series, X5-X7, Range-Rover, Cadillac Escalade)</li>\n' +
            '          <li class="list-group-item">5 класс (Mercedes-Benz Viano, Sptinter , Ford Trasit)</li>\n' +
            '        </ul>\n' +
            '</div>')
        $(function () {
          $('[data-toggle="popover"]').popover()
        })

        $("#selecthimka").change(function(){
        himkaprice = $(this).children("option:selected").val();
        });

    });



    $('#exampleModalCenter1').on('click','#himkabtn3', function () {

        if (himkaprice == 1){
            himkaprice = '2500-3500';
        };

        if (himkaprice == 2){
            himkaprice = '3000-4500';
        };

        if (himkaprice == 3){
            himkaprice = '3500-5000';
        };

        if (himkaprice == 4){
            himkaprice = '4000-6000';
        };

        if (himkaprice == 5){
            himkaprice = '6000-10000';
        };

        $('#zapis-title').html('Почти готово');
        $('#zapis-body').html('<div class="container">\n' +
            '    <h3>Ориентировочная цена на ваш автомобиль '+himkaprice+'р.</h3>\n' +
            '    <h4>Для более детального уточнения цены и записи на конкретную дату укажите свои данные</h4>\n' +
            '    <form method="post" action="/recall">\n' +
            '<input name="csrfmiddlewaretoken" hidden value="'+csrftoken+'" >'+
            '<input name="uslyga" hidden value="Полная химчистка" >'+
            '      <div class="form-group">\n' +
            '        <label for="namehimka_all">Имя</label>\n' +
            '        <input name="namehimka_all" type="text" class="form-control" id="namehimka_all">\n' +
            '      </div>\n' +
            '      <div class="form-group">\n' +
            '        <label for="phonehimka_all">Телефон</label>\n' +
            '        <input name="phonehimka_all" type="text" class="form-control" id="phonehimka_all">\n' +
            '      </div>\n' +
            '      <div class="form-group">\n' +
            '        <label for="autohimka_all">Марка и модель авто</label>\n' +
            '        <input name="autohimka_all" type="text" class="form-control" id="autohimka_all">\n' +
            '      </div>\n' +
            '      <button id="himkabtn4" type="submit" class="btn btn-primary">Отправить</button>\n' +
            '    </form>\n' +
            '</div>')
    });




    $('#exampleModalCenter1').on('click','#himkabtn5', function () {
        $('#zapis-title').html('Выберите услугу');
        $('#zapis-body').html('<div class="list-group">\n' +
            '  <button id="himkabtn1" type="button" class="list-group-item list-group-item-action">\n' +
            '    Химчистка авто\n' +
            '  </button>\n' +
            '  <button id="polirovkabtn1" type="button" class="list-group-item list-group-item-action">Полировка авто</button>\n' +
            '  <button id="krytexbtn1" type="button" class="list-group-item list-group-item-action">Нанесение защитного покрытия</button>\n' +
            '  <button id="anotherbtn" type="button" class="list-group-item list-group-item-action">Другое</button>\n' +
            '</div>');

    });

    // полировка
    $('#exampleModalCenter1').on('click','#polirovkabtn1', function () {
        $('#zapis-title').html('Какие элементы хотите отполировать');
        $('#zapis-body').html('<div class="list-group">\n' +
            '  <button id="polirovkabtnkuzov" type="button" class="list-group-item list-group-item-action">\n' +
            '    Кузов\n' +
            '  </button>\n' +
            '  <button id="polirovkabtnfari" type="button" class="list-group-item list-group-item-action">Фары</button>\n' +
            '  <button id="polirovkabtnsalon" type="button" class="list-group-item list-group-item-action">Элементы салона</button>\n' +
            '</div>');

    });

    // полировка кузова
    $('#exampleModalCenter1').on('click','#polirovkabtnkuzov', function () {
        $('#zapis-title').html('Вас интересует полировка всего кузова или конкретной детали');
        $('#zapis-body').html('<div class="row">\n' +
            '    <button id="polirovkabtnkuzov_vse" type="button" class="btn btn-primary col-6">Весь кузов</button>\n' +
            '    <button id="polirovkabtnkuzov_ne_vse" type="button" class="btn btn-secondary col-6">Отдельная деталь</button>\n' +
            '</div>');

    });

    $('#exampleModalCenter1').on('click','#polirovkabtnkuzov_ne_vse', function () {
        $('#zapis-title').html('Почти готово');
        $('#zapis-body').html('<div class="container">\n' +
            '    <h4>Для более детального уточнения цены и записи на конкретную дату укажите свои данные</h4>\n' +
            '    <form method="post" action="/recall">\n' +
            '<input name="csrfmiddlewaretoken" hidden value="'+csrftoken+'" >'+
            '<input name="uslyga" hidden value="Полировка конкретной детали" >'+
            '      <div class="form-group">\n' +
            '        <label for="namehimka_all">Имя</label>\n' +
            '        <input name="namehimka_all" type="text" class="form-control" id="namehimka_all">\n' +
            '      </div>\n' +
            '      <div class="form-group">\n' +
            '        <label for="phonehimka_all">Телефон</label>\n' +
            '        <input name="phonehimka_all" type="text" class="form-control" id="phonehimka_all">\n' +
            '      </div>\n' +
            '      <div class="form-group">\n' +
            '        <label for="autohimka_all">Марка и модель авто</label>\n' +
            '        <input name="autohimka_all" type="text" class="form-control" id="autohimka_all">\n' +
            '      </div>\n' +
            '      <button id="himkabtn4" type="submit" class="btn btn-primary">Отправить</button>\n' +
            '    </form>\n' +
            '</div>');

    });

    var polirovkaprice;

    $('#exampleModalCenter1').on('click','#polirovkabtnkuzov_vse', function () {
        $('#zapis-title').html('Выберите класс автомобиля');
        $('#zapis-body').html('<div class="container">\n' +
            '    <div class="input-group">\n' +
            '      <select class="custom-select" id="selectpolirovka" aria-label="Example select with button addon">\n' +
            '        <option selected>Выбор...</option>\n' +
            '        <option value="1">1 класс</option>\n' +
            '        <option value="2">2 класс</option>\n' +
            '        <option value="3">3 класс</option>\n' +
            '        <option value="4">4 класс</option>\n' +
            '        <option value="5">5 класс</option>\n' +
            '      </select>\n' +
            '      <div class="input-group-append">\n' +
            '        <button id="polirovkabtnselectclass" class="btn btn-outline-secondary" type="button">Далее</button>\n' +
            '      </div>\n' +
            '    </div>\n' +
            '        <ul class="list-group">\n' +
            '          <li class="list-group-item">1 класс (Kia Picanto, Hyundai Getz, Ford Fiesta, Chevrolet Spark, Citroen C1, C2c, Daewoo Matiz, Peugeot 107, 108)</li>\n' +
            '          <li class="list-group-item">2 класс (Kia Rio Ceed, Hyundai Solaris, Skoda Fabia, BMW 1-3)</li>\n' +
            '          <li class="list-group-item">3 класс (Skoda Octavia, Superb, BMW 4 5, X1-X4, Toyota Rav-4, Kia Sportage)</li>\n' +
            '          <li class="list-group-item">4 класс (BMW 7 series, X5-X7, Range-Rover, Cadillac Escalade)</li>\n' +
            '          <li class="list-group-item">5 класс (Mercedes-Benz Viano, Sptinter , Ford Trasit)</li>\n' +
            '        </ul>\n' +
            '</div>');

        $(function () {
          $('[data-toggle="popover"]').popover()
        })

        $("#selectpolirovka").change(function(){
        polirovkaprice = $(this).children("option:selected").val();
        });
    });

    $('#exampleModalCenter1').on('click','#polirovkabtnselectclass', function () {


        $('#zapis-title').html('Хотите нанести защитный состав после полировки?');
        $('#zapis-body').html('<div class="row">\n' +
            '    <button id="keramica_da" type="button" class="btn btn-primary col-6">Да</button>\n' +
            '    <button id="keramica_net" type="button" class="btn btn-secondary col-6">Нет</button>\n' +
            '</div>');


    });

    $('#exampleModalCenter1').on('click','#keramica_net', function () {
        if (polirovkaprice == 1){
            polirovkaprice = '7000-10000';
        };

        if (polirovkaprice == 2){
            polirovkaprice = '8000-12000';
        };

        if (polirovkaprice == 3){
            polirovkaprice = '10000-14000';
        };

        if (polirovkaprice == 4){
            polirovkaprice = '12000-16000';
        };

        if (polirovkaprice == 5){
            polirovkaprice = '14000-20000';
        };

        $('#zapis-title').html('Почти готово!');
        $('#zapis-body').html('<div class="container">\n' +
            '    <h3>Ориентировочная цена на ваш автомобиль '+polirovkaprice+'р.</h3>\n' +
            '    <h4>Для более детального уточнения цены и записи на конкретную дату укажите свои данные</h4>\n' +
            '    <form method="post" action="/recall">\n' +
            '<input name="csrfmiddlewaretoken" hidden value="'+csrftoken+'" >'+
            '<input name="uslyga" hidden value="Полная полировка без керамики" >'+
            '      <div class="form-group">\n' +
            '        <label for="namehimka_all">Имя</label>\n' +
            '        <input name="namehimka_all" type="text" class="form-control" id="namehimka_all">\n' +
            '      </div>\n' +
            '      <div class="form-group">\n' +
            '        <label for="phonehimka_all">Телефон</label>\n' +
            '        <input name="phonehimka_all" type="text" class="form-control" id="phonehimka_all">\n' +
            '      </div>\n' +
            '      <div class="form-group">\n' +
            '        <label for="autohimka_all">Марка и модель авто</label>\n' +
            '        <input name="autohimka_all" type="text" class="form-control" id="autohimka_all">\n' +
            '      </div>\n' +
            '      <button id="himkabtn4" type="submit" class="btn btn-primary">Отправить</button>\n' +
            '    </form>\n' +
            '</div>');


    });


    $('#exampleModalCenter1').on('click','#keramica_da', function () {

        $('#zapis-title').html('Какой защитный состав хотите нанести?');
        $('#zapis-body').html('<div class="list-group">\n' +
            '  <button id="keramica2_steklo_pokritie_select" type="button" class="list-group-item list-group-item-action">\n' +
            '    Керамика 2 слоя + слой жидкого стекла\n' +
            '  </button>\n' +
            '  <button id="keramica1_steklo_pokritie_select" type="button" class="list-group-item list-group-item-action">Керамика 1 слой + слой жидкого стекла</button>\n' +
            '  <button id="keramica1_pokritie_select" type="button" class="list-group-item list-group-item-action">Керамика 1 слой</button>\n' +
            '  <button id="steklo1_pokritie_select" type="button" class="list-group-item list-group-item-action">Жидкое стекло 1 слой</button>\n' +
            '  <button id="vosk_pokritie_select" type="button" class="list-group-item list-group-item-action">Твердый воск</button>\n' +
            '</div>');
    });

    $('#exampleModalCenter1').on('click','#keramica2_steklo_pokritie_select', function () {

        if (polirovkaprice == 1){
            polirovkaprice = '23000-32000';
        };

        if (polirovkaprice == 2){
            polirovkaprice = '24000-36000';
        };

        if (polirovkaprice == 3){
            polirovkaprice = '26000-36000';
        };

        if (polirovkaprice == 4){
            polirovkaprice = '28000-38000';
        };

        if (polirovkaprice == 5){
            polirovkaprice = '30000-42000';
        };

        $('#zapis-title').html('Почти готово');
        $('#zapis-body').html('<div class="container">\n' +
            '    <h3>Ориентировочная цена на ваш автомобиль (полировка и нанесение защитного покрытия) '+polirovkaprice+'р.</h3>\n' +
            '    <h4>Для более детального уточнения цены и записи на конкретную дату укажите свои данные</h4>\n' +
            '    <form method="post" action="/recall">\n' +
            '<input name="csrfmiddlewaretoken" hidden value="'+csrftoken+'" >'+
            '<input name="uslyga" hidden value="Полная полировка керамика 2 слоя, слой жидкого стекла" >'+
            '      <div class="form-group">\n' +
            '        <label for="namehimka_all">Имя</label>\n' +
            '        <input name="namehimka_all" type="text" class="form-control" id="namehimka_all">\n' +
            '      </div>\n' +
            '      <div class="form-group">\n' +
            '        <label for="phonehimka_all">Телефон</label>\n' +
            '        <input name="phonehimka_all" type="text" class="form-control" id="phonehimka_all">\n' +
            '      </div>\n' +
            '      <div class="form-group">\n' +
            '        <label for="autohimka_all">Марка и модель авто</label>\n' +
            '        <input name="autohimka_all" type="text" class="form-control" id="autohimka_all">\n' +
            '      </div>\n' +
            '      <button id="himkabtn4" type="submit" class="btn btn-primary">Отправить</button>\n' +
            '    </form>\n' +
            '</div>');
    });

    $('#exampleModalCenter1').on('click','#keramica1_steklo_pokritie_select', function () {

        if (polirovkaprice == 1){
            polirovkaprice = '17000-24000';
        };

        if (polirovkaprice == 2){
            polirovkaprice = '18000-28000';
        };

        if (polirovkaprice == 3){
            polirovkaprice = '20000-28000';
        };

        if (polirovkaprice == 4){
            polirovkaprice = '22000-30000';
        };

        if (polirovkaprice == 5){
            polirovkaprice = '24000-34000';
        };

        $('#zapis-title').html('Почти готово');
        $('#zapis-body').html('<div class="container">\n' +
            '    <h3>Ориентировочная цена на ваш автомобиль (полировка и нанесение защитного покрытия) '+polirovkaprice+'р.</h3>\n' +
            '    <h4>Для более детального уточнения цены и записи на конкретную дату укажите свои данные</h4>\n' +
            '    <form method="post" action="/recall">\n' +
            '<input name="csrfmiddlewaretoken" hidden value="'+csrftoken+'" >'+
            '<input name="uslyga" hidden value="Полная полировка керамика 1 слой, слой жидкого стекла" >'+
            '      <div class="form-group">\n' +
            '        <label for="namehimka_all">Имя</label>\n' +
            '        <input name="namehimka_all" type="text" class="form-control" id="namehimka_all">\n' +
            '      </div>\n' +
            '      <div class="form-group">\n' +
            '        <label for="phonehimka_all">Телефон</label>\n' +
            '        <input name="phonehimka_all" type="text" class="form-control" id="phonehimka_all">\n' +
            '      </div>\n' +
            '      <div class="form-group">\n' +
            '        <label for="autohimka_all">Марка и модель авто</label>\n' +
            '        <input name="autohimka_all" type="text" class="form-control" id="autohimka_all">\n' +
            '      </div>\n' +
            '      <button id="himkabtn4" type="submit" class="btn btn-primary">Отправить</button>\n' +
            '    </form>\n' +
            '</div>');
    });

    $('#exampleModalCenter1').on('click','#keramica1_pokritie_select', function () {

        if (polirovkaprice == 1){
            polirovkaprice = '13000-18000';
        };

        if (polirovkaprice == 2){
            polirovkaprice = '14000-22000';
        };

        if (polirovkaprice == 3){
            polirovkaprice = '16000-22000';
        };

        if (polirovkaprice == 4){
            polirovkaprice = '18000-24000';
        };

        if (polirovkaprice == 5){
            polirovkaprice = '20000-28000';
        };

        $('#zapis-title').html('Почти готово');
        $('#zapis-body').html('<div class="container">\n' +
            '    <h3>Ориентировочная цена на ваш автомобиль (полировка и нанесение защитного покрытия) '+polirovkaprice+'р.</h3>\n' +
            '    <h4>Для более детального уточнения цены и записи на конкретную дату укажите свои данные</h4>\n' +
            '    <form method="post" action="/recall">\n' +
            '<input name="csrfmiddlewaretoken" hidden value="'+csrftoken+'" >'+
            '<input name="uslyga" hidden value="Полная полировка керамика 1 слой" >'+
            '      <div class="form-group">\n' +
            '        <label for="namehimka_all">Имя</label>\n' +
            '        <input name="namehimka_all" type="text" class="form-control" id="namehimka_all">\n' +
            '      </div>\n' +
            '      <div class="form-group">\n' +
            '        <label for="phonehimka_all">Телефон</label>\n' +
            '        <input name="phonehimka_all" type="text" class="form-control" id="phonehimka_all">\n' +
            '      </div>\n' +
            '      <div class="form-group">\n' +
            '        <label for="autohimka_all">Марка и модель авто</label>\n' +
            '        <input name="autohimka_all" type="text" class="form-control" id="autohimka_all">\n' +
            '      </div>\n' +
            '      <button id="himkabtn4" type="submit" class="btn btn-primary">Отправить</button>\n' +
            '    </form>\n' +
            '</div>');
    });

    $('#exampleModalCenter1').on('click','#steklo1_pokritie_select', function () {

        if (polirovkaprice == 1){
            polirovkaprice = '11000-16000';
        };

        if (polirovkaprice == 2){
            polirovkaprice = '12000-20000';
        };

        if (polirovkaprice == 3){
            polirovkaprice = '14000-20000';
        };

        if (polirovkaprice == 4){
            polirovkaprice = '16000-22000';
        };

        if (polirovkaprice == 5){
            polirovkaprice = '18000-26000';
        };

        $('#zapis-title').html('Почти готово');
        $('#zapis-body').html('<div class="container">\n' +
            '    <h3>Ориентировочная цена на ваш автомобиль (полировка и нанесение защитного покрытия) '+polirovkaprice+'р.</h3>\n' +
            '    <h4>Для более детального уточнения цены и записи на конкретную дату укажите свои данные</h4>\n' +
            '    <form method="post" action="/recall">\n' +
            '<input name="csrfmiddlewaretoken" hidden value="'+csrftoken+'" >'+
            '<input name="uslyga" hidden value="Полная полировка, стекло 1 слой" >'+
            '      <div class="form-group">\n' +
            '        <label for="namehimka_all">Имя</label>\n' +
            '        <input name="namehimka_all" type="text" class="form-control" id="namehimka_all">\n' +
            '      </div>\n' +
            '      <div class="form-group">\n' +
            '        <label for="phonehimka_all">Телефон</label>\n' +
            '        <input name="phonehimka_all" type="text" class="form-control" id="phonehimka_all">\n' +
            '      </div>\n' +
            '      <div class="form-group">\n' +
            '        <label for="autohimka_all">Марка и модель авто</label>\n' +
            '        <input name="autohimka_all" type="text" class="form-control" id="autohimka_all">\n' +
            '      </div>\n' +
            '      <button id="himkabtn4" type="submit" class="btn btn-primary">Отправить</button>\n' +
            '    </form>\n' +
            '</div>');
    });

    $('#exampleModalCenter1').on('click','#vosk_pokritie_select', function () {

        if (polirovkaprice == 1){
            polirovkaprice = '8000-11500';
        };

        if (polirovkaprice == 2){
            polirovkaprice = '9000-13500';
        };

        if (polirovkaprice == 3){
            polirovkaprice = '11000-15500';
        };

        if (polirovkaprice == 4){
            polirovkaprice = '13000-17500';
        };

        if (polirovkaprice == 5){
            polirovkaprice = '15000-21500';
        };

        $('#zapis-title').html('Почти готово');
        $('#zapis-body').html('<div class="container">\n' +
            '    <h3>Ориентировочная цена на ваш автомобиль (полировка и нанесение защитного покрытия) '+polirovkaprice+'р.</h3>\n' +
            '    <h4>Для более детального уточнения цены и записи на конкретную дату укажите свои данные</h4>\n' +
            '    <form method="post" action="/recall">\n' +
            '<input name="csrfmiddlewaretoken" hidden value="'+csrftoken+'" >'+
            '<input name="uslyga" hidden value="Полная полировка и твердый воск" >'+
            '      <div class="form-group">\n' +
            '        <label for="namehimka_all">Имя</label>\n' +
            '        <input name="namehimka_all" type="text" class="form-control" id="namehimka_all">\n' +
            '      </div>\n' +
            '      <div class="form-group">\n' +
            '        <label for="phonehimka_all">Телефон</label>\n' +
            '        <input name="phonehimka_all" type="text" class="form-control" id="phonehimka_all">\n' +
            '      </div>\n' +
            '      <div class="form-group">\n' +
            '        <label for="autohimka_all">Марка и модель авто</label>\n' +
            '        <input name="autohimka_all" type="text" class="form-control" id="autohimka_all">\n' +
            '      </div>\n' +
            '      <button id="himkabtn4" type="submit" class="btn btn-primary">Отправить</button>\n' +
            '    </form>\n' +
            '</div>');
    });

    $('#exampleModalCenter1').on('click','#polirovkabtnfari', function () {


        $('#zapis-title').html('Какую пару фар хотите отполировать?');
        $('#zapis-body').html('<div class="row">\n' +
            '    <button id="polirovka_vseh_far_btn" type="button" class="btn btn-primary col-6">Передние и задние</button>\n' +
            '    <button id="polirovka_pered_far_btn" type="button" class="btn btn-secondary col-6">Передние</button>\n' +
            '</div>');


    });

    $('#exampleModalCenter1').on('click','#polirovka_vseh_far_btn', function () {


        $('#zapis-title').html('Почти готово');
        $('#zapis-body').html('<div class="container">\n' +
            '    <h3>Ориентировочная цена на ваш автомобиль 2000-3000 р.</h3>\n' +
            '    <h4>Для более детального уточнения цены и записи на конкретную дату укажите свои данные</h4>\n' +
            '    <form method="post" action="/recall">\n' +
            '<input name="csrfmiddlewaretoken" hidden value="'+csrftoken+'" >'+
            '<input name="uslyga" hidden value="Полировка передних и задних фар" >'+
            '      <div class="form-group">\n' +
            '        <label for="namehimka_all">Имя</label>\n' +
            '        <input name="namehimka_all" type="text" class="form-control" id="namehimka_all">\n' +
            '      </div>\n' +
            '      <div class="form-group">\n' +
            '        <label for="phonehimka_all">Телефон</label>\n' +
            '        <input name="phonehimka_all" type="text" class="form-control" id="phonehimka_all">\n' +
            '      </div>\n' +
            '      <div class="form-group">\n' +
            '        <label for="autohimka_all">Марка и модель авто</label>\n' +
            '        <input name="autohimka_all" type="text" class="form-control" id="autohimka_all">\n' +
            '      </div>\n' +
            '      <button id="himkabtn4" type="submit" class="btn btn-primary">Отправить</button>\n' +
            '    </form>\n' +
            '</div>');


    });

    $('#exampleModalCenter1').on('click','#polirovka_pered_far_btn', function () {


        $('#zapis-title').html('Почти готово');
        $('#zapis-body').html('<div class="container">\n' +
            '    <h3>Ориентировочная цена на ваш автомобиль 1000-2000 р.</h3>\n' +
            '    <h4>Для более детального уточнения цены и записи на конкретную дату укажите свои данные</h4>\n' +
            '    <form method="post" action="/recall">\n' +
            '<input name="csrfmiddlewaretoken" hidden value="'+csrftoken+'" >'+
            '<input name="uslyga" hidden value="Полировка передних фар" >'+
            '      <div class="form-group">\n' +
            '        <label for="namehimka_all">Имя</label>\n' +
            '        <input name="namehimka_all" type="text" class="form-control" id="namehimka_all">\n' +
            '      </div>\n' +
            '      <div class="form-group">\n' +
            '        <label for="phonehimka_all">Телефон</label>\n' +
            '        <input name="phonehimka_all" type="text" class="form-control" id="phonehimka_all">\n' +
            '      </div>\n' +
            '      <div class="form-group">\n' +
            '        <label for="autohimka_all">Марка и модель авто</label>\n' +
            '        <input name="autohimka_all" type="text" class="form-control" id="autohimka_all">\n' +
            '      </div>\n' +
            '      <button id="himkabtn4" type="submit" class="btn btn-primary">Отправить</button>\n' +
            '    </form>\n' +
            '</div>');


    });


    $('#exampleModalCenter1').on('click','#polirovkabtnsalon', function () {


        $('#zapis-title').html('Почти готово!');
        $('#zapis-body').html('<div class="container">\n' +
            '    <h3>Ориентировочная цена на ваш автомобиль от 1000р.</h3>\n' +
            '    <h4>Для более детального уточнения цены и записи на конкретную дату укажите свои данные</h4>\n' +
            '    <form method="post" action="/recall">\n' +
            '<input name="csrfmiddlewaretoken" hidden value="'+csrftoken+'" >'+
            '<input name="uslyga" hidden value="Полировка интереьера салона" >'+
            '      <div class="form-group">\n' +
            '        <label for="namehimka_all">Имя</label>\n' +
            '        <input name="namehimka_all" type="text" class="form-control" id="namehimka_all">\n' +
            '      </div>\n' +
            '      <div class="form-group">\n' +
            '        <label for="phonehimka_all">Телефон</label>\n' +
            '        <input name="phonehimka_all" type="text" class="form-control" id="phonehimka_all">\n' +
            '      </div>\n' +
            '      <div class="form-group">\n' +
            '        <label for="autohimka_all">Марка и модель авто</label>\n' +
            '        <input name="autohimka_all" type="text" class="form-control" id="autohimka_all">\n' +
            '      </div>\n' +
            '      <button id="himkabtn4" type="submit" class="btn btn-primary">Отправить</button>\n' +
            '    </form>\n' +
            '</div>');


    });


    var pokritie_price;

    $('#exampleModalCenter1').on('click','#krytexbtn1', function () {
        $('#zapis-title').html('Выберите класс автомобиля');
        $('#zapis-body').html('<div class="container">\n' +
            '    <div class="input-group">\n' +
            '      <select class="custom-select" id="selectpokritie" aria-label="Example select with button addon">\n' +
            '        <option selected>Выбор...</option>\n' +
            '        <option value="1">1 класс</option>\n' +
            '        <option value="2">2 класс</option>\n' +
            '        <option value="3">3 класс</option>\n' +
            '        <option value="4">4 класс</option>\n' +
            '        <option value="5">5 класс</option>\n' +
            '      </select>\n' +
            '      <div class="input-group-append">\n' +
            '        <button id="pokritiebtnselectclass" class="btn btn-outline-secondary" type="button">Далее</button>\n' +
            '      </div>\n' +
            '    </div>\n' +
            '        <ul class="list-group">\n' +
            '          <li class="list-group-item">1 класс (Kia Picanto, Hyundai Getz, Ford Fiesta, Chevrolet Spark, Citroen C1, C2c, Daewoo Matiz, Peugeot 107, 108)</li>\n' +
            '          <li class="list-group-item">2 класс (Kia Rio Ceed, Hyundai Solaris, Skoda Fabia, BMW 1-3)</li>\n' +
            '          <li class="list-group-item">3 класс (Skoda Octavia, Superb, BMW 4 5, X1-X4, Toyota Rav-4, Kia Sportage)</li>\n' +
            '          <li class="list-group-item">4 класс (BMW 7 series, X5-X7, Range-Rover, Cadillac Escalade)</li>\n' +
            '          <li class="list-group-item">5 класс (Mercedes-Benz Viano, Sptinter , Ford Trasit)</li>\n' +
            '        </ul>\n' +
            '</div>');

        $(function () {
          $('[data-toggle="popover"]').popover()
        })

        $("#selectpokritie").change(function(){
        pokritie_price = $(this).children("option:selected").val();
        });
    });

    $('#exampleModalCenter1').on('click','#pokritiebtnselectclass', function () {

        $('#zapis-title').html('Какое защитное покрытие хотите нанести');
        $('#zapis-body').html('<div class="list-group">\n' +
            '  <button id="usluga_pokritie_keramika_3" type="button" class="list-group-item list-group-item-action">\n' +
            '    Керамика 3 слоя\n' +
            '  </button>\n' +
            '  <button id="usluga_pokritie_keramika_steklo_2_1" type="button" class="list-group-item list-group-item-action">Керамика 2 слой + слой жидкого стекла</button>\n' +
            '  <button id="usluga_pokritie_keramika_steklo_1_1" type="button" class="list-group-item list-group-item-action">Керамика 1 слой + слой жидкого стекла</button>\n' +
            '  <button id="usluga_pokritie_keramika_1" type="button" class="list-group-item list-group-item-action">Керамика 1 слой</button>\n' +
            '  <button id="usluga_pokritie_steklo_1" type="button" class="list-group-item list-group-item-action">Жидкое стекло 1 слой</button>\n' +
            '  <button id="usluga_pokritie_vosk" type="button" class="list-group-item list-group-item-action">Твердый воск</button>\n' +
            '</div>');


    });

    $('#exampleModalCenter1').on('click','#usluga_pokritie_keramika_3', function () {

        if (pokritie_price == 1){
            pokritie_price = '18000-24000';
        };

        if (pokritie_price == 2){
            pokritie_price = '18000-24000';
        };

        if (pokritie_price == 3){
            pokritie_price = '21000-27000';
        };

        if (pokritie_price == 4){
            pokritie_price = '22500-28500';
        };

        if (pokritie_price == 5){
            pokritie_price = '24000-30000';
        };

        $('#zapis-title').html('Почти готово!');
        $('#zapis-body').html('<div class="container">\n' +
            '    <h3>Ориентировочная цена на ваш автомобиль (нанесение защитного покрытия) '+pokritie_price+'р.</h3>\n' +
            '    <h4>Для более детального уточнения цены и записи на конкретную дату укажите свои данные</h4>\n' +
            '    <form method="post" action="/recall">\n' +
            '<input name="csrfmiddlewaretoken" hidden value="'+csrftoken+'" >'+
            '<input name="uslyga" hidden value="Покрытие керамикой 3 слоя" >'+
            '      <div class="form-group">\n' +
            '        <label for="namehimka_all">Имя</label>\n' +
            '        <input name="namehimka_all" type="text" class="form-control" id="namehimka_all">\n' +
            '      </div>\n' +
            '      <div class="form-group">\n' +
            '        <label for="phonehimka_all">Телефон</label>\n' +
            '        <input name="phonehimka_all" type="text" class="form-control" id="phonehimka_all">\n' +
            '      </div>\n' +
            '      <div class="form-group">\n' +
            '        <label for="autohimka_all">Марка и модель авто</label>\n' +
            '        <input name="autohimka_all" type="text" class="form-control" id="autohimka_all">\n' +
            '      </div>\n' +
            '      <button id="himkabtn4" type="submit" class="btn btn-primary">Отправить</button>\n' +
            '    </form>\n' +
            '</div>');


    });

    $('#exampleModalCenter1').on('click','#usluga_pokritie_keramika_steklo_2_1', function () {

        if (pokritie_price == 1){
            pokritie_price = '16000-22000';
        };

        if (pokritie_price == 2){
            pokritie_price = '16000-22000';
        };

        if (pokritie_price == 3){
            pokritie_price = '18000-22000';
        };

        if (pokritie_price == 4){
            pokritie_price = '19000-23000';
        };

        if (pokritie_price == 5){
            pokritie_price = '20000-24000';
        };

        $('#zapis-title').html('Почти готово!');
        $('#zapis-body').html('<div class="container">\n' +
            '    <h3>Ориентировочная цена на ваш автомобиль (нанесение защитного покрытия) '+pokritie_price+'р.</h3>\n' +
            '    <h4>Для более детального уточнения цены и записи на конкретную дату укажите свои данные</h4>\n' +
            '    <form method="post" action="/recall">\n' +
            '<input name="csrfmiddlewaretoken" hidden value="'+csrftoken+'" >'+
            '<input name="uslyga" hidden value="Покрытие керамикой 2 слоя, жидкое стекло 1 слой" >'+
            '      <div class="form-group">\n' +
            '        <label for="namehimka_all">Имя</label>\n' +
            '        <input name="namehimka_all" type="text" class="form-control" id="namehimka_all">\n' +
            '      </div>\n' +
            '      <div class="form-group">\n' +
            '        <label for="phonehimka_all">Телефон</label>\n' +
            '        <input name="phonehimka_all" type="text" class="form-control" id="phonehimka_all">\n' +
            '      </div>\n' +
            '      <div class="form-group">\n' +
            '        <label for="autohimka_all">Марка и модель авто</label>\n' +
            '        <input name="autohimka_all" type="text" class="form-control" id="autohimka_all">\n' +
            '      </div>\n' +
            '      <button id="himkabtn4" type="submit" class="btn btn-primary">Отправить</button>\n' +
            '    </form>\n' +
            '</div>');


    });


    $('#exampleModalCenter1').on('click','#usluga_pokritie_keramika_steklo_1_1', function () {

        if (pokritie_price == 1){
            pokritie_price = '10000-14000';
        };

        if (pokritie_price == 2){
            pokritie_price = '10000-14000';
        };

        if (pokritie_price == 3){
            pokritie_price = '11000-13000';
        };

        if (pokritie_price == 4){
            pokritie_price = '11500-13500';
        };

        if (pokritie_price == 5){
            pokritie_price = '12000-14000';
        };

        $('#zapis-title').html('Почти готово!');
        $('#zapis-body').html('<div class="container">\n' +
            '    <h3>Ориентировочная цена на ваш автомобиль (нанесение защитного покрытия) '+pokritie_price+'р.</h3>\n' +
            '    <h4>Для более детального уточнения цены и записи на конкретную дату укажите свои данные</h4>\n' +
            '    <form method="post" action="/recall">\n' +
            '<input name="csrfmiddlewaretoken" hidden value="'+csrftoken+'" >'+
            '<input name="uslyga" hidden value="Покрытие керамикой 1 слой, жидкое стекло 1 слой" >'+
            '      <div class="form-group">\n' +
            '        <label for="namehimka_all">Имя</label>\n' +
            '        <input name="namehimka_all" type="text" class="form-control" id="namehimka_all">\n' +
            '      </div>\n' +
            '      <div class="form-group">\n' +
            '        <label for="phonehimka_all">Телефон</label>\n' +
            '        <input name="phonehimka_all" type="text" class="form-control" id="phonehimka_all">\n' +
            '      </div>\n' +
            '      <div class="form-group">\n' +
            '        <label for="autohimka_all">Марка и модель авто</label>\n' +
            '        <input name="autohimka_all" type="text" class="form-control" id="autohimka_all">\n' +
            '      </div>\n' +
            '      <button id="himkabtn4" type="submit" class="btn btn-primary">Отправить</button>\n' +
            '    </form>\n' +
            '</div>');


    });

    $('#exampleModalCenter1').on('click','#usluga_pokritie_keramika_1', function () {

        if (pokritie_price == 1){
            pokritie_price = '6000-8000';
        };

        if (pokritie_price == 2){
            pokritie_price = '6000-8000';
        };

        if (pokritie_price == 3){
            pokritie_price = '7000-9000';
        };

        if (pokritie_price == 4){
            pokritie_price = '7500-9500';
        };

        if (pokritie_price == 5){
            pokritie_price = '8000-10000';
        };

        $('#zapis-title').html('Почти готово!');
        $('#zapis-body').html('<div class="container">\n' +
            '    <h3>Ориентировочная цена на ваш автомобиль (нанесение защитного покрытия) '+pokritie_price+'р.</h3>\n' +
            '    <h4>Для более детального уточнения цены и записи на конкретную дату укажите свои данные</h4>\n' +
            '    <form method="post" action="/recall">\n' +
            '<input name="csrfmiddlewaretoken" hidden value="'+csrftoken+'" >'+
            '<input name="uslyga" hidden value="Покрытие керамикой 1 слой" >'+
            '      <div class="form-group">\n' +
            '        <label for="namehimka_all">Имя</label>\n' +
            '        <input name="namehimka_all" type="text" class="form-control" id="namehimka_all">\n' +
            '      </div>\n' +
            '      <div class="form-group">\n' +
            '        <label for="phonehimka_all">Телефон</label>\n' +
            '        <input name="phonehimka_all" type="text" class="form-control" id="phonehimka_all">\n' +
            '      </div>\n' +
            '      <div class="form-group">\n' +
            '        <label for="autohimka_all">Марка и модель авто</label>\n' +
            '        <input name="autohimka_all" type="text" class="form-control" id="autohimka_all">\n' +
            '      </div>\n' +
            '      <button id="himkabtn4" type="submit" class="btn btn-primary">Отправить</button>\n' +
            '    </form>\n' +
            '</div>');


    });


    $('#exampleModalCenter1').on('click','#usluga_pokritie_steklo_1', function () {

        if (pokritie_price == 1){
            pokritie_price = '3500-5000';
        };

        if (pokritie_price == 2){
            pokritie_price = '3500-5000';
        };

        if (pokritie_price == 3){
            pokritie_price = '4000-5000';
        };

        if (pokritie_price == 4){
            pokritie_price = '4500-5500';
        };

        if (pokritie_price == 5){
            pokritie_price = '5000-6000';
        };

        $('#zapis-title').html('Почти готово!');
        $('#zapis-body').html('<div class="container">\n' +
            '    <h3>Ориентировочная цена на ваш автомобиль (нанесение защитного покрытия) '+pokritie_price+'р.</h3>\n' +
            '    <h4>Для более детального уточнения цены и записи на конкретную дату укажите свои данные</h4>\n' +
            '    <form method="post" action="/recall">\n' +
            '<input name="csrfmiddlewaretoken" hidden value="'+csrftoken+'" >'+
            '<input name="uslyga" hidden value="Покрытие стеклом 1 слой" >'+
            '      <div class="form-group">\n' +
            '        <label for="namehimka_all">Имя</label>\n' +
            '        <input name="namehimka_all" type="text" class="form-control" id="namehimka_all">\n' +
            '      </div>\n' +
            '      <div class="form-group">\n' +
            '        <label for="phonehimka_all">Телефон</label>\n' +
            '        <input name="phonehimka_all" type="text" class="form-control" id="phonehimka_all">\n' +
            '      </div>\n' +
            '      <div class="form-group">\n' +
            '        <label for="autohimka_all">Марка и модель авто</label>\n' +
            '        <input name="autohimka_all" type="text" class="form-control" id="autohimka_all">\n' +
            '      </div>\n' +
            '      <button id="himkabtn4" type="submit" class="btn btn-primary">Отправить</button>\n' +
            '    </form>\n' +
            '</div>');


    });

    $('#exampleModalCenter1').on('click','#usluga_pokritie_vosk', function () {

        if (pokritie_price == 1){
            pokritie_price = '1500-2000';
        };

        if (pokritie_price == 2){
            pokritie_price = '1500-2000';
        };

        if (pokritie_price == 3){
            pokritie_price = '1500-2000';
        };

        if (pokritie_price == 4){
            pokritie_price = '2000-2500';
        };

        if (pokritie_price == 5){
            pokritie_price = '2000-2500';
        };

        $('#zapis-title').html('Почти готово!');
        $('#zapis-body').html('<div class="container">\n' +
            '    <h3>Ориентировочная цена на ваш автомобиль (нанесение защитного покрытия) '+pokritie_price+'р.</h3>\n' +
            '    <h4>Для более детального уточнения цены и записи на конкретную дату укажите свои данные</h4>\n' +
            '    <form method="post" action="/recall">\n' +
            '<input name="csrfmiddlewaretoken" hidden value="'+csrftoken+'" >'+
            '<input name="uslyga" hidden value="Покрытие твердым воском" >'+
            '      <div class="form-group">\n' +
            '        <label for="namehimka_all">Имя</label>\n' +
            '        <input name="namehimka_all" type="text" class="form-control" id="namehimka_all">\n' +
            '      </div>\n' +
            '      <div class="form-group">\n' +
            '        <label for="phonehimka_all">Телефон</label>\n' +
            '        <input name="phonehimka_all" type="text" class="form-control" id="phonehimka_all">\n' +
            '      </div>\n' +
            '      <div class="form-group">\n' +
            '        <label for="autohimka_all">Марка и модель авто</label>\n' +
            '        <input name="autohimka_all" type="text" class="form-control" id="autohimka_all">\n' +
            '      </div>\n' +
            '      <button id="himkabtn4" type="submit" class="btn btn-primary">Отправить</button>\n' +
            '    </form>\n' +
            '</div>');


    });
    $('#exampleModalCenter1').on('click','#anotherbtn', function () {

        $('#zapis-title').html('Почти готово!');
        $('#zapis-body').html('<div class="container">\n' +
            '    <h4>Для более детального уточнения цены и записи на конкретную дату укажите свои данные</h4>\n' +
            '    <form method="post" action="/recall">\n' +
            '<input name="csrfmiddlewaretoken" hidden value="'+csrftoken+'" >'+
            '<input name="uslyga" hidden value="Другое из услуг" >'+
            '      <div class="form-group">\n' +
            '        <label for="namehimka_all">Имя</label>\n' +
            '        <input name="namehimka_all" type="text" class="form-control" id="namehimka_all">\n' +
            '      </div>\n' +
            '      <div class="form-group">\n' +
            '        <label for="phonehimka_all">Телефон</label>\n' +
            '        <input name="phonehimka_all" type="text" class="form-control" id="phonehimka_all">\n' +
            '      </div>\n' +
            '      <div class="form-group">\n' +
            '        <label for="autohimka_all">Марка и модель авто</label>\n' +
            '        <input name="autohimka_all" type="text" class="form-control" id="autohimka_all">\n' +
            '      </div>\n' +
            '      <button id="himkabtn4" type="submit" class="btn btn-primary">Отправить</button>\n' +
            '    </form>\n' +
            '</div>');


    });