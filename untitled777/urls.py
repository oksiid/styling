"""untitled777 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from prostyling import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index, name='index'),
    path('recall', views.recall, name='recall'),
    path('reviews', views.reviews, name='reviews'),
    path('works', views.works, name='works'),
    path('work/<int:raboti_id>', views.works_detail, name='works_detail'),
    path('rev_add', views.rev_add, name='rev_add'),
    path('detailing_car_wash', views.detailing_car_wash, name='detailing_car_wash'),
    path('detailing_motor_wash', views.detailing_motor_wash, name='detailing_motor_wash'),
    path('polirovka_kuzova', views.polirovka_kuzova, name='polirovka_kuzova'),
    path('pokritie_keramica', views.pokritie_keramica, name='pokritie_keramica'),
    path('pokritie_steklo', views.pokritie_steklo, name='pokritie_steklo'),
    path('pokritie_antidojd', views.pokritie_antidojd, name='pokritie_antidojd'),
    path('polirovka_optiki', views.polirovka_optiki, name='polirovka_optiki'),
    path('himchistka', views.himchistka, name='himchistka'),
    path('zashchitnye_pokrytiya_leather', views.zashchitnye_pokrytiya_leather, name='zashchitnye_pokrytiya_leather'),
    path('zashchitnye_pokrytiya_tex', views.zashchitnye_pokrytiya_tex, name='zashchitnye_pokrytiya_tex'),
    path('polirovka_interier', views.polirovka_interier, name='polirovka_interier'),
    path('ozonirovanie', views.ozonirovanie, name='ozonirovanie'),
    path('add_raboty', views.add_raboty, name='add_raboty'),
    path('login_to', views.login_to, name='login_to'),
    path('test', views.test, name='test'),
]+ static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
